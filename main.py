from random import randint

def main():
    works = []
    jobs = {}
    sum_jobs = {}

    rand_int = len(works)
    iterable = len(works)

    # Прогон по всему списку пока не останеться одно значение
    while iterable >= 2:
        # Промежуточные прогоны от длиный списка до длиный списка умноженного на 10
        while rand_int <= len(works) * 10:
            # Подсчет по значениям
            for i in range(rand_int):
                rand_works = randint(0, len(works)-1) # Генерация случайного числа
                jobs.setdefault(works[rand_works], 0) # Дефолтное значение для ключа
                jobs[works[rand_works]] += 1 # Увеличение значения 
            rand_int += len(works) # Увеличение диапозона
        # Вывод словаря
        for key, value in jobs.items():
            print(f'{key} -> {value}')
        # Поиск наименьшего значения
        for key, value in jobs.items():
            minimal = min(jobs.values())
            if value == minimal:
                del_works = key
        # Суммирование наибольших в новом словаре
        for key, value in jobs.items():
            sum_jobs.setdefault(key, 0)
            sum_jobs[key] += value
            
        works.remove(del_works) # Удаление из списка наименьшего значения
        jobs.pop(del_works, None) # Удаление из словаря наименьшего значения

        # Обнуление значений в словаре
        for key in jobs.keys():
            jobs[key] = 0

        print()
        rand_int = 0 # Обнуление значения счетчика
        iterable -= 1 # Уменьшение цикла иттрераций

    # Вывод наибольшего значения
    for key, value in sum_jobs.items():
        maximal = max(sum_jobs.values())
        if value == maximal:
            print("Наибольшее количество совпадений ->", key, "со значением ->" ,maximal)

    # Вывод наименьшего значения
    for key, value in sum_jobs.items():
        maximal = min(sum_jobs.values())
        if value == maximal:
            print("Наименьшее количество совпадений ->", key, "со значением ->" ,maximal)
    
    print()
    
    for k, v in sum_jobs.items():
        print(f'{k} -> {v}')

if __name__ == '__main__':
    main()
